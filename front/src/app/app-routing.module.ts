import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullComponent } from './Modules/layouts/full/full.component';
export const Approutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      { path: '', redirectTo: '/auth/login', pathMatch: 'full' },

      {
        path: 'component',
        loadChildren: () => import('./Modules/pages/component.module').then(m => m.ComponentsModule),
      },
      
    
    ]
  },
  {
    path: 'auth',
    loadChildren: () => import('./Modules/authentification/auth.module').then(m => m.AuthModule)
  },
  {
    path: '**',
    redirectTo: '/auth/login'
  }
];
