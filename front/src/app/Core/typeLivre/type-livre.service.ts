import {  Injectable} from '@angular/core';

import { GeneriqueService } from '../Generiqservice/generiqueservice.service';

@Injectable({
  providedIn: 'root'
})

export class TypeLivreService {
 
  constructor(public generiqueService :GeneriqueService ) { }
 /**
   * web services to get all type Livre
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */

  public getAllTypeLivre( observe: any = 'body', reportProgress: boolean = false ) {

    return this.generiqueService.GetApi(observe,reportProgress, 'typeLivre/alltypeLivre')
  
  }



 /**
   * web services to create type Livre
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */

  public addTypeLivre( observe: any = 'body', reportProgress: boolean = false ) {

    return this.generiqueService.PostApi(observe,reportProgress, 'typeLivre/addtypeLivre')
  
  }


   /**
   * web services to delete type Livre 
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   * @param typeLivreid id type livre
   */

    public deleteTypeLivre(  typeLivreid : string , observe: any = 'body', reportProgress: boolean = false ) {

      return this.generiqueService.DeleteApi(observe,reportProgress, `typeLivre/deletetypeLivre/${typeLivreid}`)
    
    }
  

  /**
  * * web services to update type Livre 
  * 
  * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
  * @param reportProgress flag to report request and response progress.
  * @param typeLivreid id type livre
  */
  public updateTypeLivre(   typeLivreid : string  , observe: any = 'body', reportProgress: boolean = false ){
    return this.generiqueService.PutApi(observe,reportProgress, `typeLivre/updatetypeLivre/${typeLivreid}`)
   
  }



}