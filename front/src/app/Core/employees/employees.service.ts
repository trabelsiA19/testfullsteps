import { Injectable } from '@angular/core';
import { GeneriqueService } from '../Generiqservice/generiqueservice.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {


  constructor(public generiqueService :GeneriqueService ) { }
 /**
   * web services to get all Employees
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */

  public getAllEmployees( observe: any = 'body', reportProgress: boolean = false ) {

    return this.generiqueService.GetApi(observe,reportProgress, 'employes/allEmployees')
  
  }

   /**
   * web services to delete Employees 
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   * @param idEmployee id  employee
   */

    public deleteEmployes( idEmployee : string , observe: any = 'body', reportProgress: boolean = false ) {

      return this.generiqueService.DeleteApi(observe,reportProgress,`employes/deleteEmployee/${idEmployee}`)
    
    }

  /**
  * * web services to update Employees 
  * 
  * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
  * @param reportProgress flag to report request and response progress.
  * @param idEmployee id  employee
  */
  public updateEmployee(  idEmployee : string , observe: any = 'body', reportProgress: boolean = false ){
    return this.generiqueService.PutApi(observe,reportProgress,`employes/updateEmployee/${idEmployee}`)
   
  }
}
