import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import {map} from 'rxjs/operators'
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
userinfo = new BehaviorSubject(0);

  constructor(private httpClient:HttpClient,public jwtHelper: JwtHelperService) { 
    this.isUserConnectedSubject.next(this.isUserConnected())
  }
  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
 if(!token){
   return false;
 }
    return !this.jwtHelper.isTokenExpired(token);
  }

  isUserConnectedSubject = new BehaviorSubject<boolean>(false);


  loginUser(user:any){
    return this.httpClient.post(environment.PathAdresses + 'auth/signin',user).pipe(map((res:any) => {
       if(res.success == 1)
       console.log(res)
        this.saveToken(res.token);
        localStorage.setItem("nom",res.nom)
        localStorage.setItem("userid",res.userid)
        localStorage.setItem("email",res.email) 
        localStorage.setItem("role",res.role) 
      return res; 
    }));
  }


  registerUser(user:any){
    return this.httpClient.post(environment.PathAdresses + 'auth/signup',user).pipe(map((res:any) => {
      if(res.success == 1)
        this.saveToken(res.token);
      return res;
    }));
  }
  saveToken(token:any){
    localStorage.setItem("token",token);
    this.isUserConnectedSubject.next(true);
  }
  getToken(){
    let token = localStorage.getItem("token");
    return token;
  }
  isUserConnected(){
    let token = localStorage.getItem("token");
    return token!= null;
  }
  logout(){
    localStorage.clear();
    this.isUserConnectedSubject.next(false);
    window.location.replace("auth/login")

  }
}




