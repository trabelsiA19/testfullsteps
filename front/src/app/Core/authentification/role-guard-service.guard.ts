
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})

  export class RoleGuardServiceGuard implements CanActivate{
  constructor(private router:Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let token = localStorage.getItem("token");
      const helper = new JwtHelperService();
      const decodedToken = helper.decodeToken(token!);
      let roles = decodedToken.role;
      if((token)&&((roles=="admin")))
      {
        return true;
      }
      else{
        this.router.navigate(['/']);
        return false;
      }
  }
  
}

