import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';

export const BASE_PATH = new InjectionToken<string>('basePath');

@Injectable()
export class GeneriqueService {
  defaultHeaders: any;
  protected basePath = environment.PathAdresses;
  
  constructor(protected httpClient: HttpClient,@Optional()@Inject(BASE_PATH) basePath: string ,private modalService: NgbModal, public sanitizer: DomSanitizer ) { }
  
      public PostApi( observe: any = 'body', reportProgress: boolean = false , url : string): Observable<any> {
        let headers = this.defaultHeaders;
    
        // to determine the Content-Type header
        const consumes: string[] = [
        ];
       
        return this.httpClient.request<any>('post',`${this.basePath}${url}`,
          {
            headers: headers,
            body: observe,
            reportProgress: reportProgress
          }
        );
      }
      public PutApi( observe: any = 'body', reportProgress: boolean = false , url : string): Observable<any> {
        let headers = this.defaultHeaders;
    
        // to determine the Content-Type header
        const consumes: string[] = [
        ];
       
        return this.httpClient.request<any>('put',`${this.basePath}${url}`,
          {
            headers: headers,
            body: observe,
            reportProgress: reportProgress
          }
        );
      }
      public GetApi( observe: any = 'body', reportProgress: boolean = false , url : string ): Observable<any> {

   
        let headers = this.defaultHeaders;
    
        // to determine the Content-Type header
     
    
        return this.httpClient.request<any>('get',`${this.basePath}${url}`,
          {
            headers: headers,
            observe: observe,
            reportProgress: reportProgress
          }
        );
      }


      public DeleteApi( observe: any = 'body', reportProgress: boolean = false , url : string ): Observable<any> {

   
        let headers = this.defaultHeaders;
    
        // to determine the Content-Type header
     
    
        return this.httpClient.request<any>('delete',`${this.basePath}${url}`,
          {
            headers: headers,
            observe: observe,
            reportProgress: reportProgress
          }
        );
      }









}
