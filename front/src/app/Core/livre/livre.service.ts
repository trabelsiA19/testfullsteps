import { Injectable } from '@angular/core';
import { GeneriqueService } from '../Generiqservice/generiqueservice.service';

@Injectable({
  providedIn: 'root'
})
export class LivreService {


  constructor(public generiqueService :GeneriqueService ) { }
 /**
   * web services to get all Livre
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */

  public getAllLivre( observe: any = 'body', reportProgress: boolean = false ) {

    return this.generiqueService.GetApi(observe,reportProgress, 'livre/getAllLivre')
  
  }



 /**
   * web services to create Livre
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */

  public addLivre( observe: any = 'body', reportProgress: boolean = false ) {

    return this.generiqueService.PostApi(observe,reportProgress, 'livre/addLivre')
  
  }


   /**
   * web services to delete Livre 
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   * @param livreid id livre
   */

    public deleteLivre(livreid : string , observe: any = 'body', reportProgress: boolean = false ) {
      
      return this.generiqueService.DeleteApi(observe,reportProgress, `livre/deleteLivre/${livreid}`)
    
    }
  

  /**
  * * web services to update  Livre 
  * 
  * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
  * @param reportProgress flag to report request and response progress.
  * @param livreid id livre
  */
  public updateLivre(  livreid : string  ,observe: any = 'body', reportProgress: boolean = false ){
    return this.generiqueService.PutApi(observe,reportProgress, `livre/updateLivre/${livreid}`)
   
  }

}
