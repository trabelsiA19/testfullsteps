import { Injectable } from '@angular/core';
import { GeneriqueService } from '../Generiqservice/generiqueservice.service';

@Injectable({
  providedIn: 'root'
})
export class AuteursService {

  constructor(public generiqueService :GeneriqueService ) { }
 /**
   * web services to get all auteur
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */

  public getAllauteur( observe: any = 'body', reportProgress: boolean = false ) {

    return this.generiqueService.GetApi(observe,reportProgress, 'auteur/allAuteurs')
  
  }



 /**
   * web services to create auteurs
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public createAuteur( observe: any = 'body',  reportProgress: boolean = false ) {
    return this.generiqueService.PostApi(observe,reportProgress, 'auteur/addAuteur')
  
  }


   /**
   * web services to delete auteurs 
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   * @param  auteurid id auteur 
   */

    public deleteAuteur( auteurid : string ,observe: any = 'body', reportProgress: boolean = false ) {

      return this.generiqueService.DeleteApi(observe,reportProgress, `auteur/deleteAuteur/${auteurid}`)
    
    }
  

  /**
  * * web services to update auteurs
  * 
  * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
  * @param reportProgress flag to report request and response progress.
  * @param  auteurid id auteur 
  */
  public updateAuteur( auteurid : string ,  observe: any = 'body', reportProgress: boolean = false ){
    return this.generiqueService.PutApi(observe,reportProgress, `auteur/updateAuteur/${auteurid}`)
   
  }



}
