import { Component, OnInit } from '@angular/core';
import { timeout } from 'rxjs/operators';
import { EmployeesService } from 'src/app/Core/employees/employees.service';
import { Employees } from '../../shared/models/employees';


@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees:Employees[]=[];


  settings = {
    columns: {
  
   nom:
   {
     title: 'nom'
   },
   prenom:
   {
     title: 'prenom'
   },
   email:
   {
     title: 'prenom'
   },
   },   pager: {
    perPage: 7,
  },

 actions: {
  add: false,      //  if you want to remove add button

  position: 'right'  //left|right
 },

 edit:{
  editButtonContent: '<a  class="btne" href="#" ><i  class="fa fa-edit"></i></a>',
  saveButtonContent: '<a   class="btne" href="#" ><i class="fa fa-check"></i></a>',
  cancelButtonContent:'<a class="btnd" href="#" ><i class="fa fa-times"></i></a>',
  confirmSave: true,
},


 delete: {
   deleteButtonContent: '<a  class="btnd" href="#"><i class="fa fa-trash"></i></a>',
   confirmDelete: true,

 },

  }


  constructor( private employeesApiService:EmployeesService ) { }

  ngOnInit(): void {
this.getAllEmployes()
  }

  getAllEmployes(){
  this.employeesApiService.getAllEmployees().pipe(timeout(20000)).subscribe(
        (data:Employees[] )=>{
          console.log(data);
          this.employees=data;
          console.log(this.employees)
        },
        error=>{
          console.log(error);
        }
    );
  }




  
  onDeleteConfirm(event:any) {
  if (window.confirm('Voulez-vous supprimez l \'employees')) {
    event.confirm.resolve();
      this.employeesApiService.deleteEmployes(event.data._id,).subscribe( 
        (data:any) =>{ 
        console.log(data);
        },
        error =>{
          console.log(error);
        })
  }
  }

  modifier(event:any){
    this.employeesApiService.updateEmployee(event.data._id,event.newData).subscribe(
        newData=>{
          event.confirm.resolve(); 
          console.log("update",event.newData)
        },
        error => {console.log(error);}
      ) 
    }

}
