import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeLivreComponent } from './type-livre.component';

describe('TypeLivreComponent', () => {
  let component: TypeLivreComponent;
  let fixture: ComponentFixture<TypeLivreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeLivreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeLivreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
