import { Component, OnInit } from '@angular/core';
import { timeout } from 'rxjs/operators';
import { TypeLivreService } from 'src/app/Core/typeLivre/type-livre.service';
import { TypeLivre } from '../../shared/models/typeLivre';

@Component({
  selector: 'app-type-livre',
  templateUrl: './type-livre.component.html',
  styleUrls: ['./type-livre.component.css']
})
export class TypeLivreComponent implements OnInit {

  typeLivre:TypeLivre[]=[];


  settings = {
    columns: {
 
   nameTypeLivre:
   {
     title: 'Libelle Type Livre '
   },
   },   pager: {
    perPage: 7,
  },

 actions: {

  position: 'right'  //left|right
 },

 edit:{
  editButtonContent: '<a  class="btne" href="#" ><i  class="fa fa-edit"></i></a>',
  saveButtonContent: '<a   class="btne" href="#" ><i class="fa fa-check"></i></a>',
  cancelButtonContent:'<a class="btnd" href="#" ><i class="fa fa-times"></i></a>',
  confirmSave: true,
},


 delete: {
   deleteButtonContent: '<a  class="btnd" href="#"><i class="fa fa-trash"></i></a>',
   confirmDelete: true,

 },
 add:{
   addButtonContent:'<a class="btn btn-outline-info" href="#"><i class="fa fa-plus"></i></a>',
   createButtonContent: '<a  class="btne"  href="#" ><i class="fa fa-check"></i></a>',
   cancelButtonContent: '<a   class="btnd" href="#" ><i class="fa fa-times"></i></a>',
   confirmCreate: true
 }

  }


  constructor( private typeLivreApiService:TypeLivreService ) { }

  ngOnInit(): void {
        this.getAllTypeLivre()
  }

  getAllTypeLivre(){
  this.typeLivreApiService.getAllTypeLivre().pipe(timeout(20000)).subscribe(
        (data:TypeLivre[] )=>{
            this.typeLivre=data;
        },
        error=>{
          console.log(error);
        }
    );
  }






onDeleteConfirm(event:any) {

  if (window.confirm('Voulez-vous supprimez la categorie')) 
    
  {
  event.confirm.resolve();
    this.typeLivreApiService.deleteTypeLivre(event.data._id,).subscribe( 
      (data:any) =>{ 
      console.log(data);
    
      },
      error =>{
        console.log(error);
      })
}

}






ajouter(event:any){
 
  this.typeLivreApiService.addTypeLivre(event.newData).subscribe(

    newData => {
      event.confirm.resolve(event.newData);
      console.log("added",event.newData);
      
     
    }
    , 
    error => {
      console.log(error);
    
    }
    ) 

}



modifier(event:any){

    
  this.typeLivreApiService.updateTypeLivre(event.data._id,event.newData).subscribe(
      newData=>{
        event.confirm.resolve(); 
        console.log("update",event.newData)
      },
      error => {console.log(error);}
      
    ) 
  }
 



}
