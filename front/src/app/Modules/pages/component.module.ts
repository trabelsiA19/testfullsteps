import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ComponentsRoutes } from './component.routing';


import { TypeLivreComponent } from './type-livre/type-livre.component';
import { LivreComponent } from './livre/livre.component';
import { AuteursComponent } from './auteurs/auteurs.component';
import { EmployeesComponent } from './employees/employees.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Ng2CompleterModule } from "ng2-completer";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ComponentsRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule, 
    Ng2SmartTableModule, 
    Ng2CompleterModule, 
  ],
  declarations: [

    
    TypeLivreComponent,
    LivreComponent,
    AuteursComponent,
    EmployeesComponent,
  ]
})
export class ComponentsModule {}
