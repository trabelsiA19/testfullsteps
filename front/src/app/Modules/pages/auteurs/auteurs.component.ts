import { Component, OnInit } from '@angular/core';
import { timeout } from 'rxjs/operators';
import { AuteursService } from 'src/app/Core/auteurs/auteurs.service';
import { Auteur } from '../../shared/models/auteurs';

@Component({
  selector: 'app-auteurs',
  templateUrl: './auteurs.component.html',
  styleUrls: ['./auteurs.component.css']
})
export class AuteursComponent implements OnInit {

  auteurs:Auteur[]=[];


  settings = {
    columns: {
   nom:
   {
     title: 'nom'
   },
   prenom:
   {
     title: 'prenom'
   },
   age:
   {
     title: 'age'
   },
   
   },   pager: {
    perPage: 7,
  },

 actions: {

  position: 'right'  //left|right
 },

 edit:{
  editButtonContent: '<a  class="btne" href="#" ><i  class="fa fa-edit"></i></a>',
  saveButtonContent: '<a   class="btne" href="#" ><i class="fa fa-check"></i></a>',
  cancelButtonContent:'<a class="btnd" href="#" ><i class="fa fa-times"></i></a>',
  confirmSave: true,
},


 delete: {
   deleteButtonContent: '<a  class="btnd" href="#"><i class="fa fa-trash"></i></a>',
   confirmDelete: true,

 },
 add:{
   addButtonContent:'<a class="btn btn-outline-info" href="#"><i class="fa fa-plus"></i></a>',
   createButtonContent: '<a  class="btne"  href="#" ><i class="fa fa-check"></i></a>',
   cancelButtonContent: '<a   class="btnd" href="#" ><i class="fa fa-times"></i></a>',
   confirmCreate: true
 }

  }

  constructor( private auteursApiService:AuteursService ) { }
  ngOnInit(): void {
      this.getAllAuteurs()
  }

  getAllAuteurs(){
  this.auteursApiService.getAllauteur().pipe(timeout(20000)).subscribe(
        (data:Auteur[] )=>{
          console.log(data);
          this.auteurs=data;
          console.log(this.auteurs)
        },
        error=>{
          console.log(error);
        }
    );
  }

onDeleteConfirm(event:any) {
if (window.confirm('Voulez-vous supprimez l \'auteur')) {
  event.confirm.resolve();
    this.auteursApiService.deleteAuteur(event.data._id,).subscribe( 
      (data:any) =>{ 
      console.log(data);
      },
      error =>{
        console.log(error);
      })
}
}
ajouter(event:any){
  this.auteursApiService.createAuteur(event.newData).subscribe(
    newData => {
      event.confirm.resolve(event.newData);
      console.log("add",event.newData);
    }, 
    error => {
      console.log(error);
    }
  )
}
modifier(event:any){
  this.auteursApiService.updateAuteur(event.data._id,event.newData).subscribe(
      newData=>{
        event.confirm.resolve(); 
        console.log("update",event.newData)
      },
      error => {console.log(error);}
    ) 
  }
}
