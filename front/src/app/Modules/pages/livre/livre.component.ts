import { Component, OnInit } from '@angular/core';
import { timeout } from 'rxjs/operators';
import { AuteursService } from 'src/app/Core/auteurs/auteurs.service';
import { LivreService } from 'src/app/Core/livre/livre.service';
import { TypeLivreService } from 'src/app/Core/typeLivre/type-livre.service';
import { Auteur } from '../../shared/models/auteurs';
import { Livre } from '../../shared/models/livre';
import { TypeLivre } from '../../shared/models/typeLivre';

@Component({
  selector: 'app-livre',
  templateUrl: './livre.component.html',
  styleUrls: ['./livre.component.css']
})
export class LivreComponent implements OnInit {

  typeLivre:TypeLivre[]=[];
  livre:Livre[]=[];
  auteurs:Auteur[]=[]; 

  settings = {
    columns: {
 
   name:
   {
     title: 'Nom'
   },
   price:
   {
     title: 'Prix',
     type:'Number',
   },

   typelivreid:
   {
     title: 'Type de livre',
     editor:
     {
       type: 'list',
       config:
       {
         list:[{ value: '', title: '' },]
       },
     },
     valuePrepareFunction: (row:any) => {
       return row.nameTypeLivre
     },
   },
   auteurid:
   {
     title: 'Auteur', 
     editor:
     {
       type: 'list',
       config:
       {
         list:[{ value: '', title: '' },]
       },
     },
     valuePrepareFunction: (row:any) => {
       return row.nom
          },
   },
   description:
   {
     title: 'Description'
   },

   
   },   pager: {
    perPage: 7,
  },

 actions: {

  position: 'right'  //left|right
 },

 edit:{
  editButtonContent: '<a  class="btne" href="#" ><i  class="fa fa-edit"></i></a>',
  saveButtonContent: '<a   class="btne" href="#" ><i class="fa fa-check"></i></a>',
  cancelButtonContent:'<a class="btnd" href="#" ><i class="fa fa-times"></i></a>',
  confirmSave: true,
},


 delete: {
   deleteButtonContent: '<a  class="btnd" href="#"><i class="fa fa-trash"></i></a>',
   confirmDelete: true,

 },
 add:{
   addButtonContent:'<a class="btn btn-outline-info" href="#"><i class="fa fa-plus"></i></a>',
   createButtonContent: '<a  class="btne"  href="#" ><i class="fa fa-check"></i></a>',
   cancelButtonContent: '<a   class="btnd" href="#" ><i class="fa fa-times"></i></a>',
   confirmCreate: true
 }

  }


  constructor( private livreApiService:LivreService,private typeLivreApiService:TypeLivreService ,private auteursApiService:AuteursService  ) { }

  ngOnInit(): void {
        this.getAllLivre(); 
        this.getAllTypeLivre();  
        this.getAllAuteurs(); 
  }

  getAllLivre(){
  this.livreApiService.getAllLivre().pipe(timeout(20000)).subscribe(
        (data:any[] )=>{
          this.livre=data;
        },
        error=>{
          console.log(error);
        }
    );
  }

onDeleteConfirm(event:any) {
  if (window.confirm('Voulez-vous supprimez la livre')) {
    event.confirm.resolve();
      this.livreApiService.deleteLivre(event.data._id,).subscribe( 
        (data:any) =>{ 
        console.log(data);
        },
        error =>{
          console.log(error);
        })
  }
  }
  ajouter(event:any){
    this.livreApiService.addLivre(event.newData).subscribe(
      newData => {
        event.confirm.resolve(event.newData);
 
        location.reload()
            }, 
      error => {
        console.log(error);
      }
    )
  }
  modifier(event:any){
    this.livreApiService.updateLivre(event.data._id,event.newData).subscribe(
        newData=>{
          event.confirm.resolve(); 
          location.reload()
        },
        error => {console.log(error);}
      ) 
    }
   

    getAllAuteurs(){
      this.auteursApiService.getAllauteur().pipe(timeout(20000)).subscribe(
            (data:Auteur[] )=>{
              this.auteurs=data;
              for (const l of this.auteurs) {
                this.settings.columns.auteurid.editor.config.list.push({value: l._id,title: l.nom });
                this.settings = Object.assign({}, this.settings);
              }
            },
            error=>{
              console.log(error);
            }
        );

        
      }
     

      getAllTypeLivre(){
        this.typeLivreApiService.getAllTypeLivre().pipe(timeout(20000)).subscribe(
              (data:TypeLivre[] )=>{
                  this.typeLivre=data;
                  for (const l of this.typeLivre) {
                    this.settings.columns.typelivreid.editor.config.list.push({"value": l._id, "title": l.nameTypeLivre });
                    this.settings = Object.assign({}, this.settings);
                  }
            
                },

              
              error=>{
                console.log(error);
              }
          );
        }
}
