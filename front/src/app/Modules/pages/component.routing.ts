import { Routes } from '@angular/router';
import { AuteursComponent } from './auteurs/auteurs.component';
import { EmployeesComponent } from './employees/employees.component';
import { LivreComponent } from './livre/livre.component';
import { TypeLivreComponent } from './type-livre/type-livre.component';
import { AuthGuardService  as AuthGuard } from '../../Core/authentification/auth-guard.service';
import { RoleGuardServiceGuard } from 'src/app/Core/authentification/role-guard-service.guard';


export const ComponentsRoutes: Routes = [
	{
		path: '',
		children: [
	

			{
				path: 'type-Livre',
				component: TypeLivreComponent,
				data: {
					title: 'Type Livre',
					
				},
				canActivate: [AuthGuard]
			},
			{
				path: 'livre',
				component: LivreComponent,
				data: {
					title: 'Livre',
					
				},
				canActivate: [AuthGuard]
				
			},
			{
				path: 'auteurs',
				component: AuteursComponent,
				data: {
					title: 'auteurs',
					
				}, 
				canActivate: [AuthGuard]
				
			},
			
			{
				path: 'employees',
				component: EmployeesComponent,
				data: {
					title: 'employees',
					
				}, 
				canActivate:[RoleGuardServiceGuard]
				
			},
			
			
		


		
		]
	}
];
