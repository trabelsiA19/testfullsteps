import { Component, EventEmitter, Output } from '@angular/core';
import { AuthService } from 'src/app/Core/authentification/auth.service';
//declare var $: any;

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent {
  @Output()
  toggleSidebar = new EventEmitter<void>();

  public showSearch = false;

  constructor(
    private authService: AuthService
  ) {

  
  }

  logout(){
    this.authService.logout(); 
  }
}
