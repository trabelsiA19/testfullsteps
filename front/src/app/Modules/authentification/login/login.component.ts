import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Core/authentification/auth.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: any = {};

  constructor(
    private http: HttpClient,
    private router: Router,
   private authService: AuthService 
  ) { }

  ngOnInit(): void {
  }
  login() {
   this.authService.loginUser(this.user).subscribe(
     
    (res:any) => {

      this.authService.userinfo.next(res.fuser);
      if (!res.success) {
        this.alertWarning("Merci de verfier vos informations")
      } else {
        this.router.navigate(['/component/livre'])
      }
    })  
    
  }

  alertWarning(error: any) {

    swal.fire({
      title: '',
      icon: 'warning',
      html: error,
      showConfirmButton: true,
      showCloseButton: true,
      confirmButtonText: 'Fermer',
      confirmButtonColor: '#e75e5e',
    })
  }
}


