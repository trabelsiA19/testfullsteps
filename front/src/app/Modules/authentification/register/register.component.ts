import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Core/authentification/auth.service';
import swal from 'sweetalert2';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  constructor(
    private authService:AuthService,

    private router: Router,

    ) { }
  user:any = {};
  ngOnInit(): void {
  }
  register(){
    console.log(this.user)
     this.authService.registerUser(this.user).subscribe(
       (res:any) => {
        this.router.navigate(['/'])
    }, 
    error=>{
      if (error.status === 402 || error?.error?.status === 402) {
      this.alertWarning(error?.error.message)
      }

    })
 
  }

  alertWarning(error: any) {
    swal.fire({
      title: '',
      icon: 'warning',
      html: error,
      showConfirmButton: true,
      showCloseButton: true,
      confirmButtonText: 'Fermer',
      confirmButtonColor: '#e75e5e',
    })
  }

}
