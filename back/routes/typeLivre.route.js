


const router = require("express").Router();
const typeLivreController = require("../controllers/typeLivre.controller");
 router.delete("/deletetypeLivre/:typeLivreid",typeLivreController.deleteTypeLivre);
router.put("/updatetypeLivre/:typeLivreid",typeLivreController.updateTypeLivre);
router.get("/findtypeLivre/:typeLivreid",typeLivreController.getTypeLivre); 
router.get("/alltypeLivre",typeLivreController.getall);
router.post("/addtypeLivre",typeLivreController.createTypeLivre);
module.exports = router