const router = require("express").Router();
const auteursController = require("../controllers/auteurs.controller");
router.delete("/deleteAuteur/:auteurid",auteursController.deleteAuteur);
router.put("/updateAuteur/:auteurid",auteursController.updateAuteur);
router.get("/findAuteur/:auteurid",auteursController.getAuteur); 
router.get("/allAuteurs",auteursController.getall);
router.post("/addAuteur",auteursController.createAuteur);
module.exports = router