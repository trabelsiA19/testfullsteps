const router = require("express").Router();
const livreController = require("../controllers/livre.controller");
router.post("/addLivre",livreController.createLivre);
router.get("/getLivre",livreController.getLivre);
router.delete("/deleteLivre/:livreid",livreController.deleteLivre);
router.put("/updateLivre/:livreid",livreController.updateLivre);
router.get("/getAllLivre",livreController.getall);
module.exports = router