const router = require("express").Router();
const employeesController = require("../controllers/employees.controller");
router.delete("/deleteEmployee/:idEmployee",employeesController.deleteEmployee);
router.put("/updateEmployee/:idEmployee",employeesController.updateEmployee);
router.get("/findEmployee/:idEmployee",employeesController.getEmployee); 
router.get("/allEmployees",employeesController.getall);
module.exports = router