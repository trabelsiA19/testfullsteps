const TypeLivre = require("../models/typeLivre.model");

const mongoose = require("mongoose");


/* creation type livre */

module.exports.createTypeLivre = async (req, res) => {
    console.log(req.body)
    const newTypeLivre = new TypeLivre({
        nameTypeLivre: req.body.nameTypeLivre,
    })
    const savedTypeLivre = await newTypeLivre.save();
    return res.status(200).json({
        success: true,
        message: "Type Livre Saved Succeffuly",
        savedTypeLivre
    })
} 

 



module.exports.getTypeLivre= async (req, res) => {
    const typesLivres = await TypeLivre.find({})
    return res.status(200).json({
        success: true,
        data: typesLivres
    })
}





/* delete  type livre  */
module.exports.deleteTypeLivre = async (req, res) => {
    let typeLivreid =  req.params.typeLivreid;
    await TypeLivre.find({ _id: typeLivreid }).exec(async (err, ress) => {
        if (ress.length !== 0) {
            await TypeLivre.findByIdAndRemove(typeLivreid);
            return res.status(200).json({
                success: true,
                message: "type Livre Deleted Successfuly",
            })
        }
        else {
            return res.status(400).json({
                success: false,
                message: "Error type livre  not Found"
            })
        }
    })
}



module.exports.updateTypeLivre = async (req, res) => {
    let typeLivreid =  req.params.typeLivreid;
    let newTypeLivre = req.body.nameTypeLivre
    await TypeLivre.findByIdAndUpdate(typeLivreid, { nameTypeLivre: newTypeLivre });
    if (typeLivreid !== null && typeLivreid !== undefined) {
        return res.status(200).json({
            success: true,
            message: "type livre  updated"
        })
    }
    else {
        return res.status(400).json({ success: false, error: "not changed" })
    }
} 

/* get all type livre  */
module.exports.getall = async (req, res) => {
    let xx = await TypeLivre.find().populate()
    return res.send(xx)
}



