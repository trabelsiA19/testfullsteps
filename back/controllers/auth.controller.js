
const User = require("../models/user.model");

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const dotenv = require("dotenv")
const saltRounds = 10;

module.exports.signup = async (req, res) => {
   const email = await User.findOne({ email: req.body.email });
  if (email) {
    res.status(402).json({ message: "email already in use" })
  } else {
    console.log(req.body)
    const password = await bcrypt.hash(req.body.password, saltRounds);
    const newuser = await User.create({
      nom: req.body.nom,
      prenom: req.body.prenom,
      email:req.body.email,
      role:req.body.role, 
      password: password,
    });
  
    res.send(newuser); 
  }
}

module.exports.signin = async (req, res) => {
  console.log(req.body);
  try {
    const user = await User.findOne({ email: req.body.email });
    console.log(user);
    if (user) {
      const cmp = await bcrypt.compare(req.body.password, user.password);
      if (cmp) {
        const token = jwt.sign({ _id: user._id, role: user.role }, process.env.TOKEN_SECRET)
        console.log(user)
         res.header("auth-token", token).send({
          token: token,
          userid:user._id,
          nom:user.nom,
          email:user.email,
          role:user.role,
          success: true
        }) 

      } else {
        res.send({success:false,message:"Wrong email or password."});
      }
    } else {  
      res.send({success:false,message:"Wrong email or password."});
    }
  } catch (error) {
    console.log(error);
    res.status(500).send("Internal Server error Occured");
  }
}



