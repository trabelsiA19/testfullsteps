const Auteur = require("../models/auteurs.model");

const mongoose = require("mongoose");


/* creation Auteur */

module.exports.createAuteur = async (req, res) => {
    console.log(req.body)
    const newAuteur = new Auteur(req.body)
    const savedAuteur = await newAuteur.save();
    return res.status(200).json({
        success: true,
        message: "Auteurs Saved Succeffuly",
        savedAuteur
    })
} 

 



module.exports.getAuteur= async (req, res) => {
    const auteur = await Auteur.find({})
    return res.status(200).json({
        success: true,
        data: auteur
    })
}





/* delete  Auteur  */
module.exports.deleteAuteur = async (req, res) => {
    let auteurid =  req.params.auteurid;
    await Auteur.find({ _id: auteurid }).exec(async (err, ress) => {
        if (ress.length !== 0) {
            await Auteur.findByIdAndRemove(auteurid);
            return res.status(200).json({
                success: true,
                message: "Auteur Deleted Successfuly",
            })
        }
        else {
            return res.status(400).json({
                success: false,
                message: "Error Auteur  not Found"
            })
        }
    })
}



module.exports.updateAuteur = async (req, res) => {
    let auteurid =  req.params.auteurid;
    let newnomAuteur = req.body.nom
    let newprenomAuteur = req.body.prenom
    let newageAuteur = req.body.age
    await Auteur.findByIdAndUpdate(auteurid, { nom: newnomAuteur , prenom:newprenomAuteur,age:newageAuteur });
    if (auteurid !== null && auteurid !== undefined) {
        return res.status(200).json({
            success: true,
            message: "auteur updated"
        })
    }
    else {
        return res.status(400).json({ success: false, error: "not changed" })
    }
} 

/* get all type livre  */
module.exports.getall = async (req, res) => {
    let xx = await Auteur.find().populate()
    return res.send(xx)
}



