const Employee = require("../models/user.model");

const mongoose = require("mongoose");






module.exports.getEmployee= async (req, res) => {
    const employee = await Employee.find({})
    return res.status(200).json({
        success: true,
        data: employee
    })
}





/* delete  Employee  */
module.exports.deleteEmployee = async (req, res) => {
    let idEmployee =  req.params.idEmployee;
    await Employee.find({ _id: idEmployee }).exec(async (err, ress) => {
        if (ress.length !== 0) {
            await Employee.findByIdAndRemove(idEmployee);
            return res.status(200).json({
                success: true,
                message: "Employee Deleted Successfuly",
            })
        }
        else {
            return res.status(400).json({
                success: false,
                message: "Error Employee  not Found"
            })
        }
    })
}

module.exports.updateEmployee = async (req, res) => {
    let idEmployee =  req.params.idEmployee;
    let newnomEmployee = req.body.nom
    let newprenomEmployee = req.body.prenom
    let newemailEmployee = req.body.email
    await Employee.findByIdAndUpdate(idEmployee, { nom: newnomEmployee , prenom:newprenomEmployee,email:newemailEmployee });
    if (idEmployee !== null && idEmployee !== undefined) {
        return res.status(200).json({
            success: true,
            message: "Employee updated"
        })
    }
    else {
        return res.status(400).json({ success: false, error: "not changed" })
    }
} 

/* get all Employee  */
module.exports.getall = async (req, res) => {
    let xx = await Employee.find({ role: "employee" }).populate()
    return res.send(xx)
}





