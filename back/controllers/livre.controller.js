const Livre = require("../models/livre.models");



module.exports.createLivre = async (req, res) => {
    const newLivre = new Livre(req.body)
    const savedLivre = await newLivre.save();
    return res.status(200).json({
        success: true,
        savedLivre
    })
}


module.exports.deleteLivre = async (req, res) => {
    const livreid = req.params.livreid;
    await Livre.find({ _id: livreid }).exec(async (err, ress) => {
        if (ress.length !== 0) {
            await Livre.findByIdAndRemove(livreid);
            return res.status(200).json({
                success: true,
                message: "Livre Deleted Successfuly",
            })
        }
        else {
            return res.status(400).json({
                success: false,
                error: "Error Livre not Found"
            })
        }
    })
}

module.exports.updateLivre = async (req, res) => {
    let livreid = req.params.livreid
    let newlivre = req.body
    try {
        await Livre.findByIdAndUpdate(livreid, newlivre);
        if (livreid !== null && livreid !== undefined) {
            return res.status(200).json({
                success: true,
                message: "livre updated"
            })
        }
        else {
            return res.status(400).json({ success: false, error: "not changed" })
        }
    } catch (error) {
        res.status(404).json({ msg: "livre not found" })
    }
}




module.exports.getLivre = async (req, res) => {
    const livres = await Livre.find();
    return res.status(200).json({
        success: true,
        livres
    })
}




module.exports.getall = async (req, res) => {
    let xx = await Livre.find().populate("typelivreid" ).populate("auteurid")
    return res.send(xx)
}
