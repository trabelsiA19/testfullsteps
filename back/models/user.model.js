const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({

    nom: {
        type: String,
        require: true
    },
    prenom: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    role: {
        type: String,
        require: true
    },
     
},{timestamps: true})

module.exports = mongoose.model("User",userSchema);