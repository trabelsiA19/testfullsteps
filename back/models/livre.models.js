const mongoose = require("mongoose");
Schema = mongoose.Schema;
const livreSchema = new mongoose.Schema({
    name: {
        type: String
    },
    price: {
        type: Number
    },
    typelivreid:{
        type:Schema.Types.ObjectId, ref: 'TypeLivre'
    },
    auteurid : {

        type:Schema.Types.ObjectId, ref: 'Auteur'
    },
    description:{
        type: String
    },

},{timestamps: true})

module.exports = mongoose.model("Livre",livreSchema);