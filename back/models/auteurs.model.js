const mongoose = require("mongoose");


const auteurSchema = new mongoose.Schema({

    nom: {
    type: String,
    require: true
},
 prenom: {
            type: String,
            require: true
        },
    age :{
        type: Number,
        require: true
    }
}, { timestamps: true })

module.exports = mongoose.model("Auteur", auteurSchema);