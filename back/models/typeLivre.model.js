const mongoose = require("mongoose");


const typeLivreSchema = new mongoose.Schema({

    nameTypeLivre: {
    type: String,
    require: true
},
}, { timestamps: true })

module.exports = mongoose.model("TypeLivre", typeLivreSchema);