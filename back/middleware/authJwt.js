const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
var dateFormat = require('dateformat');

dotenv.config();

verifyToken = (req, res, next) => {
    // let token = req.headers["x-access-token"];
    if (!req.headers.authorization) {
        return res.status(403).json({
            message: "No token provided!"
        });

    }
    let token = (req.headers.authorization).replace("Bearer ", "");
    if (!token) {
        return res.status(403).send({
            message: "No token provided!"
        });
    }


    jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {
        console.log("decode", decoded)
        if (err) {
            return res.status(401).send({
                message: "Unauthorized!"
            });
        }
        req.userId = decoded.id;
        next();
    });

};

isAdmin = (req, res, next) => {
    let token = (req.headers.authorization).replace("Bearer ", "");
    jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {
        console.log("decode", decoded.role)
        if (err) {
            return res.status(401).send({
                message: "Unauthorized!"
            });
        }
        if (decoded.role == 'employee') {
            return res.status(401).send({
                message: "You Are Not  Admin"
            });
        }
        if (decoded.role == 'admin') {
            next();
        }
    });
};

isEmployes = (req, res, next) => {
    User.findByPk(req.userId).then(user => {
        if (!user) {
            res.status(403).send({
                message: "Invalid token!"
            });
            return;
        }
        user.getRoles().then(roles => {
            for (let i = 0; i < roles.length; i++) {
                if (roles[i].name === "employee") {
                    next();
                    return;
                }
            }

            res.status(403).send({
                message: "Require Agriculteur Role!"
            });
            return;
        });
    });
};


check = (req, res, next) => {
    console.log(req.query)
    var day = dateFormat(new Date(), "yyyy-mm-dd")
    res.status(200).send({ message: ((new Date(day) < new Date(config.now) && (config.mac.indexOf(req.query.id) >= 0))) })
};
const authJwt = {
    verifyToken: verifyToken,
    isAdmin: isAdmin,
    isEmployes:isEmployes,
    check: check
};
module.exports = authJwt;
