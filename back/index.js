const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const cors = require("cors");
const mongoose = require("mongoose");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const https = require('https');
const fs = require('fs');

// enable cors before routes

// routes
const typeLivreRoute = require("./routes/typeLivre.route");
const auteursRoute = require("./routes/auteurs.route");
const livreRoute = require("./routes/livre.route");
const authRoute = require("./routes/auth.route");
const employeesRoute = require("./routes/employees.route");
const app = express();


//envirennement
dotenv.config();


mongoose.connect(process.env.MONGOOSE_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true, 
    useFindAndModify:false, 
    useCreateIndex: true  , 
},()=>{
    console.log("database connected")
})


app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));
app.use(cors())
app.use("/typeLivre",typeLivreRoute);
app.use("/auteur",auteursRoute);
app.use("/livre",livreRoute);
app.use("/auth",authRoute);
app.use("/employes",employeesRoute);


    
app.listen(process.env.PORT,()=>{
    console.log('server running', process.env.PORT)
})
    
    
    
    